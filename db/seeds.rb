# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'csv'
require 'date'

stations = {}
CSV.table("db/result.csv").each do |st|
  station = Station.new({
    number: st[:stn_id],
    name: st[:name],
    lat: st[:lat],
    lon: st[:lon],
    group: st[:group],
  })
  stations[station.number] = station
  station.save!
end
Dir.glob("db/rain_groups/*") do |csv_file|
  CSV.foreach(csv_file) do |line|
    values = line.map.with_index do |val, idx|
      (idx == 0) ? val.to_i : val.to_f
    end
    values[1..-1].each.with_index do |val, idx|
      Weather.create({
        station_id: stations[values[0]].id,
        date: Date.new(2013, idx + 1, 1),
        precipitation: val,
      })
    end
  end
end
