class CreateStations < ActiveRecord::Migration
  def change
    create_table :stations do |t|
      t.integer :number
      t.string :name
      t.float :lat
      t.float :lon
      t.integer :group

      t.timestamps
    end
  end
end
