class CreateWeathers < ActiveRecord::Migration
  def change
    create_table :weathers do |t|
      t.references :station, index: true
      t.date :date
      t.float :precipitation

      t.timestamps
    end
  end
end
