require 'test_helper'

class WeatherTest < ActiveSupport::TestCase
  test "create weather" do
    weather = Weather.new({
      time: DateTime.new(2014, 1, 1, 12, 0, 0),
      station_id: 1,
      pressure: 1000.0,
      sea_level_pressure: 1010.0,
      temperature: 15.8,
      group: 1
    })
    assert_equal weather.group, 1
    assert_equal weather.station_id, 1
    assert_equal weather.temperature, 15.8
    assert_equal weather.pressure, 1000.0
    assert_equal weather.sea_level_pressure, 1010.0
  end
end
