MyApp::Application.routes.draw do
  # api
  get 'stations', to: 'stations#index'
  get 'stations/:id', to: 'stations#show'
  get 'weathers', to: 'weathers#index'
  get 'weathers/:id', to: 'weathers#show'

  get 'search/stations', to: 'stations#search'
  get 'search/weathers', to: 'weathers#search'

  # static page
  get 'pages/:action', to: 'pages#:action'
end
