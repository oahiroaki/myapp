class Station < ActiveRecord::Base
  has_many :weathers

  def self.search(params)
    stations = Station.all
    if params.include? :group
      stations = stations.where(group: params[:group])
    end
    if params.include :name
      stations = stations.where(name: params[:name])
    end
    stations
  end
end
