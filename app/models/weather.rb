class Weather < ActiveRecord::Base
  belongs_to :station

  scope :between_date, lambda {|s, e| where('time > ? AND time < ?', s, e)}

  scope :by_group, lambda {|group_id|
    joins(:station).where(stations: {group: group_id})
  }

  def self.search(params)
    weathers = Weather.all
    if params.include? :station_id
      weathers = weathers.where('station_id = ?', params[:station_id])
    end
    if %i(date_begin date_end).all? {|k| params.include? k}
      weathers = weathers.between_date(params[:date_begin], params[:date_end])
    end
    if params.include? :group_id
      weathers = weathers.by_group(params[:group_id])
    end
    weathers
  end
end
