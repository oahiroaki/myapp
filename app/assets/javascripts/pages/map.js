var App = {
  colors: {
    1: "#000", 2: "#333", 3: "#666", 4: "#F00", 5: "#0F0",
    6: "#00F", 7: "#AA0", 8: "#A0A", 9: "#0AA", 10: "#F60",
    11: "#F06", 12: "#06F", 13: "#0F6", 14: "#6F0", 15: "#60F",
    16: "#990", 17: "#909", 18: "#099", 19: "#A30", 20: "#3A0",
    21: "#A03", 22: "#30A", 23: "#0A3", 24: "#03A", 25: "#999",
  }
}


/*
  Station class
*/
var Station = (function() {
  function Station(data) {
    this.id = data.id || 0
    this.number = data.number || 0
    this.name = data.name || ''
    this.lat = data.lat || 0.0
    this.lon = data.lon || 0.0
    this.group = data.group || 0
    this.weathers = []
  }

  Station.prototype.getDeferWeathers = function() {
    var self = this
    function convertWeathers(weathers) {
      weathers.forEach(function(d) {
        d.date = d3.time.format("%Y-%m-%d").parse(d.date)
      })
      // sort data by date
      return weathers.sort(function(a, b) {return a.date - b.date})
    }
    var defer = $.Deferred()
    if (self.weathers.length === 0) {
      $.getJSON("/stations/" + self.id + ".json", function(data) {
        self.weathers = convertWeathers(data)
        defer.resolve(self.weathers)
      })
    } else {
      defer.resolve(self.weathers)
    }
    return defer.promise()
  }

  return Station
})();


/*
  Stations class
*/
var Stations = (function() {
  function Stations(stations) {
    this.elements = stations
  }

  // convert all stations to geojson format
  Stations.prototype.toGeoJSON = function() {
    var geojson = {}
    geojson.type = "FeatureCollection"
    geojson.features = []
    for (var key in this.elements) {
      var station = this.elements[key]
      geojson.features.push({
        type: "Feature",
        properties: {
          id: station.id,
          group: station.group
        },
        geometry: {
          type: "Point",
          coordinates: [station.lon, station.lat]
        }
      })
    }
    return geojson
  }

  return Stations
})()

/*
  Group class
*/
var Group = (function() {
  function Group(id, stations) {
    this.id = id
    this.showState = true
    this.stations = stations
  }

  Group.prototype.toLayer = function() {
    var self = this
      , colors = App.colors
      , geoJson = self.stations.toGeoJSON()

    function onClick(e) {
      var prop = e.target.feature.properties
      App.info.showStation(self.stations.elements[prop.id])
      App.ctrl.updateGroups()
    }

    return L.geoJson(geoJson, {
      pointToLayer: function(feature, latlng) {
        return L.circleMarker(latlng, {
          radius: 10,
          color: colors[feature.properties.group],
          fillColor: colors[feature.properties.group]
        })
      },
      onEachFeature: function(feature, layer) {
        layer.on('click', onClick)
      }
    })
  }

  return Group
}())


App.graph = {
  show: function(group, stnID) {
    $("#graph").show()
    this.makeGroup(group, stnID)
  },
  hide: function() {
    $("#graph")
      .empty()
      .hide()
  },
  makeGroup: function(group, stationID) {
    $.getJSON("/search/weathers.json?group_id=" + group.id)
    .done(function(weathers) {
      var sums = {}
      weathers.forEach(function(weather) {
        if (! sums.hasOwnProperty(weather.station_id)) {
          sums[weather.station_id] = []
        }
        sums[weather.station_id].push(weather.precipitation)
      })
      var arr = Object.keys(sums).map(function(stnID) {
        return sums[stnID].reduce(function(a, b) {
          return a + b
        })
      })
      var maxStnID = Object.keys(sums)[arr.indexOf(Math.max.apply(null, arr))]
      var minStnID = Object.keys(sums)[arr.indexOf(Math.min.apply(null, arr))]
      var width = $(window).width() - 360
        , height = $(window).height() - 100
        , margin = {top: 50, left: 50, right: 50, bottom: 50}
      // real width and height of graph area
      var graphWidth = width - margin.left - margin.right
        , graphHeight = height - margin.top - margin.bottom
      var x = d3.time.scale().range([0, graphWidth])
        , y = d3.scale.linear().range([graphHeight, 0])
      var color = d3.scale.category10()
      var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom")
      var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
      var line = d3.svg.line()
        .interpolate("basis")
        .x(function(d) {return x(d.date)})
        .y(function(d) {return y(d.precipitation)})
      var maxStation = group.stations.elements[maxStnID]
        , minStation = group.stations.elements[minStnID]
        , nowStation = group.stations.elements[stationID];
      var svg = d3.select("#graph")
        .append("svg")
          .attr("width", width)
          .attr("height", height)
        .append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

      $.when(
        maxStation.getDeferWeathers(),
        minStation.getDeferWeathers(),
        nowStation.getDeferWeathers())
      .done(function(maxWeathers, minWeathers, nowWeathers) {
        color.domain(["max", "min", "now"])
        var source = [
          {name: "max", values: maxWeathers},
          {name: "min", values: minWeathers},
          {name: "now", values: nowWeathers}
        ]

        x.domain(d3.extent(maxWeathers, function(d) {return d.date}))

        y.domain([
          d3.min(source, function(c) {
            return d3.min(c.values, function(v) {return v.precipitation})
          }),
          d3.max(source, function(c) {
            return d3.max(c.values, function(v) {return v.precipitation})
          })
        ])

        svg
          .append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + graphHeight + ")")
            .call(xAxis)
        svg
          .append("g")
            .attr("class", "y axis")
            .call(yAxis)
          .append("text")
            //.attr("transform", "rotate(-90)")
            .attr("x", 10)
            .attr("y", 5)
            .attr("dy", ".51em")
            .style("text-anchor", "start")
            .text("precipitation")
        var weather = svg.selectAll(".weathers")
            .data(source)
          .enter().append("g")
            .attr("class", "weathers")
        weather
          .append("path")
            .attr("class", "line")
            .attr("d", function(d) {return line(d.values)})
            .style("stroke", function(d) {return color(d.name)})
      })
    })
  }
}


App.info = {
  showStation: function(station) {
    $("#info").empty()
    $("#info").append(
      "<h3>" + station.name + "</h3>" +
      "<span id='station-id' style='display: none'>" + station.id + "</span>" +
      "<dl id='station-info'>" +
        "<dt>Lattitude</dt>" +
        "<dd class='lat'>" + station.lat + "</dd>" +
        "<dt>Longitude</dt>" +
        "<dd class='lon'>" + station.lon + "</dd>" +
        "<dt>Group</dt>" +
        "<dd class='group'>" + station.group + "</dd>" +
      "</dl>"
    )
    station.getDeferWeathers()
    .done(function(weathers) {
      var width = 300
        , height = 300
        , margin = {top: 30, bottom: 40, left: 90, right: 10}
      var graphWidth = width - margin.left - margin.right
        , graphHeight = height - margin.top - margin.bottom
      var x = d3.scale.linear()
        .range([0, graphWidth])
      var y = d3.time.scale()
        .range([0, graphHeight])
      var xAxis = d3.svg.axis()
        .scale(x)
        .orient("top")
        .ticks([5])
      var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
      var rectHeight = ~~(graphHeight / weathers.length)
      x.domain(d3.extent(weathers.map(function(d) {return d.precipitation})))
      y.domain(d3.extent(weathers.map(function(d) {return d.date})))

      var svg = d3.select("#info")
        .append("svg")
          .attr("width", width)
          .attr("height", height)
        .append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
      svg
        .append("g")
          .attr("class", "x axis")
          .call(xAxis)
      svg
        .append("g")
          .attr("class", "y axis")
          .attr("transform", "translate(0," + (rectHeight/2) + ")")
          .call(yAxis)
      svg
        .selectAll(".bar")
          .data(weathers)
        .enter().append("rect")
          .attr("class", "bar")
          .attr("y", function(d) {return y(d.date)})
          .attr("height", rectHeight)
          .attr("x", function(d) {return 0})
          .attr("width", function(d) {
            return x(d.precipitation)
          })
          .style("fill", "blue")
          .style("opacity", "0.8")
    })
  }
}


App.map = {
  map: L.mapbox.map('map', 'oahiroaki.h64geb30'),
  // stations groups
  groups: {},
  layers: {},

  initialize: function(data) {
    var self = this
    self.map.setView([38.00, 140.00], 7)
    L.control.scale().addTo(self.map)

    data.forEach(function(d) {
      if (! self.groups.hasOwnProperty(d.group)) {
        self.groups[d.group] = {}
      }
      self.groups[d.group][d.id] = new Station(d)
    })
    for (var id in self.groups) {
      self.groups[id] = new Group(id, new Stations(self.groups[id]))
    }
    return this
  },
  showGroup: function(groupID) {
    this.groups[groupID].showState = true
    if (! this.layers.hasOwnProperty(groupID)) {
      this.layers[groupID] = this.groups[groupID].toLayer()
    }
    this.map.addLayer(this.layers[groupID])
    return this
  },
  hideGroup: function(groupID) {
    this.groups[groupID].showState = false
    this.map.removeLayer(this.layers[groupID])
    return this
  },
  showAsState: function() {
    for (var id in this.groups) {
      if (this.groups[id].showState)
        this.showGroup(this.groups[id].id)
      else
        this.hideGroup(this.groups[id].id)
    }
    return this
  },
  showAllGroup: function() {
    for (var id in this.groups) {
      this.showGroup(id)
    }
    return this
  },
  hideAllGroup: function() {
    for (var id in this.groups) {
      this.hideGroup(id)
    }
    return this
  }
}


App.ctrl = {
  stations: (function() {
    var dom = $("<div>")
      .attr("id", "ctrl-stations")

    $("<button>")
      .addClass("show")
      .text("Show All Stations")
      .on("click", function() {
        App.map.showAllGroup()
        App.ctrl.updateGroups()
      })
    .appendTo(dom)

    $("<button>")
      .text("Hide All Stations")
      .on("click", function() {
        App.map.hideAllGroup()
        App.ctrl.updateGroups()
      })
    .appendTo(dom)
    return dom
  }()),
  graph: (function() {
    var dom = $("<div>")
      .attr("id", "ctrl-graph")
    $('<button>')
      .addClass("show")
      .text("Show Graph")
      .on("click", function() {
        var grpID = $("#station-info .group").text()
        var stnID = $("#station-id").text()
        App.graph.show(App.map.groups[grpID], stnID)
      })
    .appendTo(dom)
    $('<button>')
      .addClass("hide")
      .text("Hide Graph")
      .on("click", function() {
        App.graph.hide()
      })
    .appendTo(dom)
    return dom
  }()),
  groups: null,
  initGroups: function(groups) {
    var table = $("<table>").attr("id", "group-table")
      , trList = []
      , i = -1
    Object.keys(groups).forEach(function(groupID, idx) {
      if ((idx % 5) === 0) {
        i++
        trList.push($("<tr>"))
      }
      $("<td>")
        .text(groupID)
        .appendTo(trList[i])
        .attr("id", "ctrl-group-" + groupID)
        .on("click", function() {
          // reverse state
          if (App.map.groups[groupID].showState) {
            $(this).css({fontWeight: "normal"})
            App.map.groups[groupID].showState = false
          } else {
            $(this).css({fontWeight: "bold"})
            App.map.groups[groupID].showState = true
          }
          App.map.showAsState()
        })
        .css({
          backgroundColor: App.colors[groupID],
          color: "#fff",
          width: "40px",
          fontWeight: "bold",
          height: "15px",
          cursor: "pointer"
        })
    })
    trList.forEach(function(tr) {tr.appendTo(table)})
    return table
  },
  updateGroups: function() {
    for (var id in App.map.groups) {
      if (App.map.groups[id].showState)
        $("#ctrl-group-" + id).css({fontWeight: "bold"})
      else
        $("#ctrl-group-" + id).css({fontWeight: "normal"})
    }
  },
  initialize: function() {
    var self = this
    self.groups = self.initGroups(App.map.groups)
    $("#ctrl")
      .append(self.graph)
      .append(self.stations)
      .append(self.groups)
  }
}


var documentReady = $.Deferred();
$(document).ready(documentReady.resolve);


/*
  main
*/
$.when(documentReady, $.getJSON("/stations.json"))
.then(function($, data) {
  App.map.initialize(data[0])
  App.ctrl.initialize()
  App.map.showAllGroup()
  App.graph.hide()
})
