class StationsController < ApplicationController
  def index
    @stations = Station.all
    respond_to do |format|
      format.json
    end
  end

  def search
    @stations = Station.search(station_params)
    respond_to do |format|
      format.json {render action: 'index'}
    end
  end

  def show
    @station = Station.find(params[:id])
    @weathers = Weather.where(station_id: params[:id])
    respond_to do |format|
      format.html
      format.json
    end
  end

  private

  def station_params
    params.permit(
      :lon, :lat, :name, :group, :number,
      :lon_begin, :lon_end, :lat_begin, :lat_end, :format)
  end
end
