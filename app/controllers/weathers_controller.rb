class WeathersController < ApplicationController
  def index
    @weathers = Weather.all
    respond_to do |format|
      format.json
    end
  end

  def search
    @weathers = Weather.search(weather_params)
    respond_to do |format|
      format.json {render action: 'index'}
    end
  end

  def show
    @weather = Weather.find(params[:id])
    respond_to do |format|
      format.html
      format.json
    end
  end

  private

  def weather_params
    params.permit(:station_id, :group_id, :date_begin, :date_end)
  end
end
